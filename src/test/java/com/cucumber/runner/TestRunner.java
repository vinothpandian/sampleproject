package com.cucumber.runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import com.cucumber.Baseclass.BaseClass;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;




@RunWith(Cucumber.class)
@CucumberOptions(features="src\\test\\java\\com\\cucumber\\feature",
		glue ="com\\cucumber\\stepdefinition",
				plugin= {"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:Report/reports.html"}
		)

public class TestRunner {
public static WebDriver driver;

	@BeforeClass
public static  void browserLaunch() {
	driver = BaseClass.browserLaunch("chrome");
}

	
	@AfterClass
	public static void tearDown() {
		driver.close();
	}
}
